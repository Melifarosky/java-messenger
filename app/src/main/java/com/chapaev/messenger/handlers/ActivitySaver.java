package com.chapaev.messenger.handlers;

import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

/**
 * Created by Chapaev on 29.03.2017.
 */

public class ActivitySaver extends AppCompatActivity {
    public static void saveActivity(SharedPreferences prefs, Class activity) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("lastActivity", activity.getName());
        Log.d("wat", activity.getName());
        editor.commit();
    }
}
