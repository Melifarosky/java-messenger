package com.chapaev.messenger.handlers;

/**
 * Created by Chapaev on 29.03.2017.
 */

public interface ExecuteFunction {
    void execute();
}
