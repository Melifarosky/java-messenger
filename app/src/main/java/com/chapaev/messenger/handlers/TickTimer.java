package com.chapaev.messenger.handlers;

import android.os.CountDownTimer;

/**
 * Created by Chapaev on 29.03.2017.
 */

public class TickTimer extends CountDownTimer {
    private final ExecuteFunction executeFunction;
    /**
     * @param millisInFuture    The number of millis in the future from the call
     *                          to {@link #start()} until the countdown is done and {@link #onFinish()}
     *                          is called.
     * @param countDownInterval The interval along the way to receive
     *                          {@link #onTick(long)} callbacks.
     */
    public TickTimer(long millisInFuture, long countDownInterval, ExecuteFunction executeFunction) {
        super(millisInFuture, countDownInterval);
        this.executeFunction = executeFunction;
    }

    @Override
    public void onTick(long millisUntilFinished) {
        executeFunction.execute();
    }

    @Override
    public void onFinish() {}
}
