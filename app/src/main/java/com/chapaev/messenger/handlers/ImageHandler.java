package com.chapaev.messenger.handlers;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.widget.ImageView;

import com.bumptech.glide.Glide;

import java.io.ByteArrayOutputStream;
import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by Chapaev on 21.03.2017.
 */

public class ImageHandler {
    private AppCompatActivity activity;
    private static final int REQUEST_CAMERA = 1;
    private static final int SELECT_FILE = 2;

    private static final int REQUEST_STORAGE_PERMISSION = 1;

    public void getImage(AppCompatActivity appCompatActivity) {
        activity = appCompatActivity;
        chooseImage();
    }

    public void checkPermissions(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_STORAGE_PERMISSION) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                openPhotoSelect();
            }
        }
    }

    private void chooseImage() {
        final String[] items = {"Take Photo", "Choose from Library", "Cancel"};
        final String FILE_NAME = "tempCameraPhoto";

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setTitle("Add Photo");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                            REQUEST_STORAGE_PERMISSION);
                    dialog.dismiss();
                    return;
                }
                if (ContextCompat.checkSelfPermission(activity, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                            REQUEST_STORAGE_PERMISSION);
                    dialog.dismiss();
                    return;
                }
                switch (items[which]) {
                    case "Take Photo":
                        File photo = null;
                        if (android.os.Environment.getExternalStorageState().equals(
                                android.os.Environment.MEDIA_MOUNTED)) {
                            photo = new File(android.os.Environment
                                    .getExternalStorageDirectory(), FILE_NAME);
                        }

                        if (photo != null) {
                            Intent cameraIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            //selectedFile = Uri.fromFile(photo);
                            activity.startActivityForResult(cameraIntent, REQUEST_CAMERA);
                        }
                        break;
                    case "Choose from Library":
                        openPhotoSelect();
                        break;
                    default:
                        dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void openPhotoSelect() {
        Intent choosePhotoIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        choosePhotoIntent.setType("image/*");
        activity.startActivityForResult(
                Intent.createChooser(choosePhotoIntent, "Select file"),
                SELECT_FILE);
    }

    public Object setImage(@NonNull ImageView preview, int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            Bitmap image;
            if (requestCode == REQUEST_CAMERA) {
                image = (Bitmap) data.getExtras().get("data");
                preview.setImageBitmap(image);
                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                image.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                return baos.toByteArray();
            } else if (requestCode == SELECT_FILE) {
                Uri selectedImageUri = data.getData();
                String imagePath = getRealPathFromUri(selectedImageUri);
                image = BitmapFactory.decodeFile(imagePath);
                Glide.with(activity)
                        .load(selectedImageUri)
                        .into(preview);
                return selectedImageUri;
            }
        }
        return null;
    }

    private String getRealPathFromUri(Uri contentUri) {
        String selectedImagePath = null;
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity.getContentResolver().query(contentUri, projection, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            selectedImagePath = cursor.getString(column_index);
        }
        cursor.close();
        return selectedImagePath;
    }
}
