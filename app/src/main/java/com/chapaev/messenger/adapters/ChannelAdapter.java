package com.chapaev.messenger.adapters;

import android.app.Activity;
import android.content.Context;
import android.media.Image;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.chapaev.messenger.R;
import com.chapaev.messenger.model.ChannelItem;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.Date;
import java.util.List;

/**
 * Created by Chapaev on 18.03.2017.
 */

public class ChannelAdapter extends ArrayAdapter<ChannelItem> {
    Context context;

    public ChannelAdapter(@NonNull Context context, @LayoutRes int resource, List<ChannelItem> items) {
        super(context, resource, items);
        this.context = context;
    }

    private class ViewHolder {
        ImageView channelImg;
        TextView textTitle;
        TextView joiners;
        TextView fixed;
        TextView timestamp;
    }

    @Override
    @NonNull
    public View getView(int pos, View convertView, @NonNull ViewGroup parent) {
        ViewHolder holder = null;
        ChannelItem chItem = getItem(pos);

        LayoutInflater mInflater = (LayoutInflater) context
                .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.channel_item, null);
            holder = new ViewHolder();
            holder.textTitle = (TextView) convertView.findViewById(R.id.title);
            holder.channelImg = (ImageView) convertView.findViewById(R.id.icon);
            holder.joiners = (TextView) convertView.findViewById(R.id.joinersLabel);
            holder.fixed = (TextView) convertView.findViewById(R.id.fixedLabel);
            holder.timestamp = (TextView) convertView.findViewById(R.id.timestamp);
            convertView.setTag(holder);
        } else
            holder = (ViewHolder) convertView.getTag();

        holder.textTitle.setText(chItem != null ? chItem.toString() : null);
        if (chItem.getImgUrl() != null) {
            StorageReference imgRef = FirebaseStorage.getInstance().getReference()
                    .child("messenger/images/" + chItem.getImgUrl());
            Glide.with(context)
                    .using(new FirebaseImageLoader())
                    .load(imgRef)
                    .skipMemoryCache(true)
                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .signature(new StringSignature(String.valueOf(new Date())))
                    .crossFade()
                    .fitCenter()
                    .error(R.drawable.noimage)
                    .into(holder.channelImg);
        }
        if (chItem.isChannelFixed())
            holder.fixed.setVisibility(View.VISIBLE);
        holder.joiners.setText("Joiners: " + chItem.getMembersCount());
        Log.d("count", String.valueOf(chItem.getMembersCount()));
        holder.timestamp.setText("Last message  " + chItem.getFormattedTime());
        return convertView;
    }
}
