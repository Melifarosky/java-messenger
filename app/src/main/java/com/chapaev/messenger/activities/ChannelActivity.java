package com.chapaev.messenger.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.chapaev.messenger.R;
import com.chapaev.messenger.adapters.ChannelAdapter;
import com.chapaev.messenger.handlers.ActivitySaver;
import com.chapaev.messenger.model.ChannelItem;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by Chapaev on 17.03.2017.
 */

public class ChannelActivity extends AppCompatActivity {
    private static final long MAX_CHANNEL_LIFECYCLE = 20 * 60 * 1000;

    private DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private DatabaseReference chats;
    private LinkedHashMap<String, ChannelItem> channels;
    private ChannelAdapter channelAdapter;
    private ChildEventListener channelsListener;
    private View footerView;
    private View headerView;
    private Context context = this;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channels);
        Glide.get(this).clearMemory();
        chats = mDatabase.child("channels").child("chats");
        channels = new LinkedHashMap<>();

        channelsChildListener();
        guiElementListeners();

        showGravity("Loading channels...", Toast.LENGTH_SHORT);
        chats.orderByChild("channelFixed").addChildEventListener(channelsListener);
    }

    private void channelsChildListener() {
        channelsListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final ChannelItem item = dataSnapshot.getValue(ChannelItem.class);
                item.setId(dataSnapshot.getKey());
                channels.put(item.getId(), item);
                sortChannels(null);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                final ChannelItem item = dataSnapshot.getValue(ChannelItem.class);
                item.setId(dataSnapshot.getKey());
                channels.put(item.getId(), item);
                sortChannels(null);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                channels.remove(dataSnapshot.getKey());
                Log.e("removed", dataSnapshot.getKey());
                sortChannels(null);
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Channel", databaseError.getMessage());
                showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
            }
        };
    }

    private void guiElementListeners() {
    /*final Button profileButton = (Button) findViewById(R.id.btnProfile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChannelActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });*/

        final SearchView searchField = (SearchView) findViewById(R.id.searchField);
        searchField.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty()) {
                    sortChannels(null);
                    return false;
                }

                List<ChannelItem> tempChannels = new ArrayList<>(channels.values());
                LinkedList<ChannelItem> searchChannels = new LinkedList<>();
                for (ChannelItem item : tempChannels) {
                    if (Pattern.matches(".*(\\Q"+newText+"\\E).*", item.getTitle())) {
                        searchChannels.add(item);
                    }
                }
                sortChannels(searchChannels);
                return false;
            }
        });
    }

    private void sortChannels(List<ChannelItem> refChannels) {
        if (channels == null) return;
        if (channels.size() > 0) {
            List<ChannelItem> tempChannels;
            tempChannels = new ArrayList<>(refChannels == null ? channels.values() : refChannels);
            Collections.sort(tempChannels, new Comparator<ChannelItem>() {
                @Override
                public int compare(ChannelItem o1, ChannelItem o2) {
                    if (o1.isChannelFixed() && !o2.isChannelFixed()) return 1;
                    if (!o1.isChannelFixed() && o2.isChannelFixed()) return -1;
                    if (o1.getLongTimestamp() > o2.getLongTimestamp()) return 1;
                    if (o1.getLongTimestamp() < o2.getLongTimestamp()) return -1;
                    return 0;
                }
            });

            Collections.reverse(tempChannels);
            for (ChannelItem chItem : tempChannels) {
                if (chItem.isChannelFixed()) continue;
                if (System.currentTimeMillis() - chItem.getLongTimestamp() >= MAX_CHANNEL_LIFECYCLE) {
                    mDatabase.child("channels/chats").child(chItem.getTitle()).setValue(null);
                    mDatabase.child("channels/messages").child(chItem.getTitle()).setValue(null);
                    channels.remove(chItem);
                }
            }
            loadChannels(tempChannels);
        }
    }

    private void loadChannels(final List<ChannelItem> refChannels) {
        ListView channelsView = (ListView) findViewById(R.id.channelsView);
        channelsView.removeFooterView(footerView);
        channelsView.removeHeaderView(headerView);
        headerView = new View(context);
        footerView = ((LayoutInflater) ChannelActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.footer, null, false);
        //footerView = new View(context);
        channelsView.addHeaderView(headerView);
        channelsView.addFooterView(footerView);

        if (refChannels == null) return;
        channelAdapter = new ChannelAdapter(context, R.layout.channel_item, refChannels);
        channelsView.setAdapter(channelAdapter);
        channelsView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, final View view, int position, long id) {
                final String chatId = refChannels.get(position - 1).getId();
                Intent intent = new Intent(ChannelActivity.this, ChatActivity.class);
                intent.putExtra("chatId", chatId);
                intent.putExtra("fixed", refChannels.get(position - 1).isChannelFixed());
                startActivity(intent);
            }
        });

        final Button profileButton = (Button) findViewById(R.id.btnProfile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ChannelActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });
    }

    private void showGravity(String text, int type) {
        Toast toast = Toast.makeText(getApplicationContext(), text, type);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.channel_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.btnCreateChannel:
                Intent intent = new Intent(ChannelActivity.this, CreateChannelActivity.class);
                startActivity(intent);
                return true;
            case R.id.btnRefresh:
                sortChannels(null);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onStop() {
        super.onStop();

        ActivitySaver.saveActivity(getSharedPreferences("X", MODE_PRIVATE), ChannelActivity.class);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (channelsListener != null)
            chats.removeEventListener(channelsListener);
    }
}
