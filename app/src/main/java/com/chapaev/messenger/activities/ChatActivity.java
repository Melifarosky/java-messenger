package com.chapaev.messenger.activities;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.chapaev.messenger.R;
import com.chapaev.messenger.handlers.ExecuteFunction;
import com.chapaev.messenger.handlers.ImageHandler;
import com.chapaev.messenger.handlers.TickTimer;
import com.chapaev.messenger.model.BadWords;
import com.chapaev.messenger.model.Member;
import com.chapaev.messenger.model.Message;
import com.chapaev.messenger.model.User;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ServerValue;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.LinkedHashMap;

import javax.security.auth.login.LoginException;

import co.intentservice.chatui.ChatView;
import co.intentservice.chatui.models.ChatMessage;

/**
 * Created by Chapaev on 19.03.2017.
 */

public class ChatActivity extends AppCompatActivity {
    private static final String TAG = "Chat";
    private static final long INTERVAL = 12 * 60 / 100 * 1000;  // milliseconds: 20 min, 60 seconds, 100 percent, 1000 milliseconds
    private static final long MIN_TIMESTAMP = 20 * 60 * 1000;  // milliseconds: 20 min, 60 seconds, 1000 milliseconds

    private LinkedHashMap<String, Message> messages;

    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private final DatabaseReference mDatabase = FirebaseDatabase.getInstance().getReference();
    private final StorageReference storageRef = FirebaseStorage.getInstance().getReference();
    private User user;
    private final Context context = this;
    private Query messRef;
    private Query membersRef;
    private ChildEventListener membersListener;
    private ChildEventListener messChildListener;
    private ChatView chatView;
    private String chatId;
    private boolean chatFixed;
    private ImageHandler imageHandler;
    private ImageView preview;
    private Object image;
    private TickTimer timer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.chat);

        if (firebaseUser == null) try {
            throw new LoginException("Invalid firebase user!");
        } catch (LoginException e) {
            e.printStackTrace();
            return;
        }
        //Glide.get(this).clearDiskCache();

        mDatabase.child("users").child(firebaseUser.getUid())
                .addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                user = dataSnapshot.getValue(User.class);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("Channel", databaseError.getMessage());
                showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
            }
        });

        messages = new LinkedHashMap<>();
        chatView = (ChatView) findViewById(R.id.chat_view);

        imageHandler = new ImageHandler();

        preview = (ImageView) findViewById(R.id.preview);
        chatId = getIntent().getStringExtra("chatId");
        chatFixed = getIntent().getBooleanExtra("fixed", false);

        Log.d(TAG, "Chat ID = " + chatId);

        if (chatId != null) {
            mDatabase.child("channels").child("members").child(chatId).child(firebaseUser.getUid())
                    .setValue(new Member(false)).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("Joining", "Error: " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("Joining", "Success!");
                    handleMember('+');
                }
            });

            loadMessages();

            chatView.setOnSentMessageListener(new ChatView.OnSentMessageListener() {
                @Override
                public boolean sendMessage(ChatMessage chatMessage) {
                    DatabaseReference ref = messRef.getRef().push();
                    if (image != null) {
                        uploadImage(chatMessage, ref);
                        return false;
                    }
                    chatMessage.setMessage(BadWords.getFilteredText(chatMessage.getMessage()));
                    Message message = new Message(firebaseUser.getUid(), chatMessage.getMessage(), user.getAvatarUrl(), null);
                    ref.setValue(message);
                    mDatabase.child("channels").child("chats").child(chatId).child("timestamp").setValue(ServerValue.TIMESTAMP);
                    return false;
                }
            });

            chatView.setTypingListener(new ChatView.TypingListener() {
                @Override
                public void userStartedTyping() {
                    Log.d("Typing", "Yo!");
                    userIsTyping(true);
                }

                @Override
                public void userStoppedTyping() {
                    Log.d("Stopped typing", "Not Yo!");
                    userIsTyping(false);
                }
            });

            ImageButton attachBtn = (ImageButton) findViewById(R.id.attachFile);
            attachBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageHandler.getImage(ChatActivity.this);
                }
            });

            timer = new TickTimer(Long.MAX_VALUE, INTERVAL, new ExecuteFunction() {
                @Override
                public void execute() {
                    chatView.adapterHandler(INTERVAL);
                    if (chatView.getMessagesCount() == 0) {
                        deleteChannel();
                    }
                }
            });
            mDatabase.child("channels/messages").child(chatId).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getChildrenCount() == 0 && chatView.getMessagesCount() == 0)
                        deleteChannel();
                    timer.start();
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e("ChatActivity", databaseError.getMessage());
                    showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
                    timer.start();
                }
            });
        } else {
            startActivity(new Intent(ChatActivity.this, ChannelActivity.class));
            finish();
        }
    }

    void deleteChannel() {
        if (chatFixed) return;
        mDatabase.child("channels").child("chats").child(chatId).setValue(null);
        mDatabase.child("channels").child("messages").child(chatId).setValue(null);
        showGravity("Channel " + chatId + " has been deleted!", Toast.LENGTH_LONG);
        finish();
    }

    private void uploadImage(final ChatMessage chatMessage, final DatabaseReference ref) {
        StorageReference imagesRef = storageRef.child("messenger/images").child(ref.getKey());
        UploadTask uploadTask;
        if (image instanceof Uri) {
            uploadTask = imagesRef.putFile((Uri) image);
        } else if (image instanceof byte[]) {
            uploadTask = imagesRef.putBytes((byte[]) image);
        } else try {
            throw new Exception("Invalid image instance");
        } catch (Exception e) {
            e.printStackTrace();
            showGravity(e.getMessage(), Toast.LENGTH_LONG);
            return;
        }
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                showGravity("Successfully loaded image!", Toast.LENGTH_SHORT);
                image = null;
                Message message = new Message(firebaseUser.getUid(), chatMessage.getMessage(), user.getAvatarUrl(), ref.getKey());
                ref.setValue(message);
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showGravity("Storage error: " + e.getMessage(), Toast.LENGTH_SHORT);
                Log.e("Profile", "Storage error: " + e.getMessage());
                image = null;
            }
        });
    }

    private void loadMessages() {
        messRef = mDatabase.child("channels").child("messages").child(chatId);
        membersRef = mDatabase.child("channels").child("members").child(chatId);

        chatListener();
        membersListener();
    }

    private void chatListener() {
        messChildListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                final Message message = dataSnapshot.getValue(Message.class);
                if (messages.keySet().contains(dataSnapshot.getKey())) return;
                messages.put(dataSnapshot.getKey(), message);

                ChatMessage.Type msgType =
                        (message.getUserId()).equals(firebaseUser.getUid())
                                ? ChatMessage.Type.SENT
                                : ChatMessage.Type.RECEIVED;
                StorageReference avatarRef = null;
                StorageReference imageRef = null;
                if (message.getAvatarUrl() != null)
                    avatarRef = storageRef.child("messenger/avatars/"+message.getAvatarUrl());
                if (message.getImgUrl() != null) {
                    imageRef = storageRef.child("messenger/images/"+message.getImgUrl());

                }
                ChatMessage chatMessage = new ChatMessage(context, message.getMessage(), avatarRef, imageRef,
                        message.getLongTimestamp(), msgType);
                chatView.addMessage(chatMessage);
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ChatActivity", databaseError.getMessage());
                showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}
        };
    }

    private void membersListener() {
        membersListener = new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d("joining", "Someone has joined");
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                //TODO: show textlabel someone is writing
                Log.d("writing", "Someone is writing");
                if (!dataSnapshot.getKey().equals(firebaseUser.getUid())) {
                    TextView typingLabel = (TextView) findViewById(R.id.typingLabel);
                    if ((Boolean) dataSnapshot.child("typing").getValue())
                        typingLabel.setVisibility(View.VISIBLE);
                    else
                        typingLabel.setVisibility(View.GONE);
                }

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Log.d("leaving", "Someone has left");
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {}

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ChatActivity", databaseError.getMessage());
                showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
            }
        };
    }

    private void userIsTyping(boolean state) {
        mDatabase.child("channels").child("members").child(chatId).child(firebaseUser.getUid())
                .setValue(new Member(state));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imageHandler.checkPermissions(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        image = imageHandler.setImage(preview, requestCode, resultCode, data);
    }

    private void showGravity(String text, int type) {
        Toast toast = Toast.makeText(getApplicationContext(), text, type);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    private void handleMember(final char operator) {
        final DatabaseReference memRef = mDatabase.child("channels").child("chats").child(chatId).child("membersCount");
        memRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                long value = (long) dataSnapshot.getValue();
                if (operator == '+')
                    value++;
                else if (operator == '-')
                    value--;
                else return;

                memRef.setValue(value);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                Log.e("ChatActivity", databaseError.getMessage());
                showGravity(databaseError.getMessage(), Toast.LENGTH_SHORT);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        //Glide.get(this).clearMemory();
        messRef.orderByChild("timestamp").startAt(System.currentTimeMillis() - MIN_TIMESTAMP)
                .addChildEventListener(messChildListener);
        membersRef.addChildEventListener(membersListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        if (messChildListener != null)
            messRef.removeEventListener(messChildListener);
        if (membersListener != null)
            membersRef.removeEventListener(membersListener);
    }

    @Override
    public void onDestroy() {
       super.onDestroy();
        if (chatId != null) {
            mDatabase.child("channels").child("members").child(chatId).child(firebaseUser.getUid())
                    .setValue(null).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.e("Leaving", "Error: " + e.getMessage());
                }
            }).addOnSuccessListener(new OnSuccessListener<Void>() {
                @Override
                public void onSuccess(Void aVoid) {
                    Log.d("Leaving", "Success!");
                    handleMember('-');
                }
            });
        }
        if (timer != null)
            timer.cancel();
    }
}
