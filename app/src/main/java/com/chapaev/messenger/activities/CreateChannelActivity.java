package com.chapaev.messenger.activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.chapaev.messenger.handlers.ActivitySaver;
import com.chapaev.messenger.handlers.ImageHandler;
import com.chapaev.messenger.R;
import com.chapaev.messenger.model.ChannelItem;
import com.chapaev.messenger.model.Message;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.common.hash.Hashing;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageException;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.nio.charset.Charset;

/**
 * Created by Chapaev on 18.03.2017.
 */

public class CreateChannelActivity extends AppCompatActivity {
    private DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference();
    private StorageReference storageRef = FirebaseStorage.getInstance().getReference();

    private Object image;
    private ImageHandler imageHandler;

    private ImageView preview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.channel_create);

        imageHandler = new ImageHandler();
        preview = (ImageView) findViewById(R.id.preview);

        guiElementListeners();
    }

    private void guiElementListeners() {
        final Button channelButton = (Button) findViewById(R.id.footerChannels);
        channelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateChannelActivity.this, ChannelActivity.class);
                startActivity(intent);
            }
        });

        final Button profileButton = (Button) findViewById(R.id.btnProfile);
        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(CreateChannelActivity.this, ProfileActivity.class);
                startActivity(intent);
            }
        });

        final Button button1 = (Button) findViewById(R.id.btnChooseFile);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imageHandler.getImage(CreateChannelActivity.this);
            }
        });

        final Button button2 = (Button) findViewById(R.id.btnApplyCreateChannel);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createChannel();
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imageHandler.checkPermissions(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        image = imageHandler.setImage((ImageView) findViewById(R.id.preview), requestCode, resultCode, data);
    }

    private void createChannel() {
        final DatabaseReference channelsRef = databaseRef.child("channels/chats");
        final String title = ((EditText) findViewById(R.id.channelTitleText)).getText().toString();
        channelsRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot ds : dataSnapshot.getChildren()) {
                    if (((String) ds.getKey()).toLowerCase().equals(title.toLowerCase())) {
                        showJoinToChannelDialog(ds.getValue(ChannelItem.class));
                        return;
                    }
                }
                try {
                    uploadData(channelsRef, title);
                } catch (StorageException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                showGravity("Database error: " + databaseError.getMessage(), Toast.LENGTH_LONG);
                Log.e("GetChannels", "Database error: " + databaseError.getMessage());
            }
        });
    }

    private void uploadData(final DatabaseReference channelsRef, final String title) throws StorageException {
        final String fileName = Hashing.sha1().hashString(title, Charset.defaultCharset()).toString();
        StorageReference imagesRef = storageRef.child("messenger/images").child(fileName);
        UploadTask uploadTask;
        if (image instanceof Uri) {
            uploadTask = imagesRef.putFile((Uri) image);
        } else if (image instanceof byte[]) {
            uploadTask = imagesRef.putBytes((byte[]) image);
        } else try {
            throw new Exception("Invalid image instance");
        } catch (Exception e) {
            Log.e("CreateChannelActivity", e.getMessage());
            showGravity(e.getMessage(), Toast.LENGTH_LONG);
            return;
        }
        showGravity("Creating channel..." ,Toast.LENGTH_SHORT);

        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showGravity("Can't create channel: " + e.getMessage(), Toast.LENGTH_LONG);
                Log.e("UploadImage", e.getMessage());
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                final ChannelItem chItem = new ChannelItem(title, fileName);
                channelsRef.child(title).setValue(chItem).addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        databaseRef.child("channels").child("messages").child(title).push()
                                .setValue(new Message("admin", "Created channel", "admin.png", null))
                                .addOnCompleteListener(new OnCompleteListener<Void>() {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task) {
                                        showGravity("Successfully created channel!", Toast.LENGTH_LONG);
                                        Log.d("ChannelCreation", "Successfully created channel!");
                                        Intent intent = new Intent(CreateChannelActivity.this, ChatActivity.class);
                                        intent.putExtra("chatId", title);
                                        intent.putExtra("fixed", chItem.isChannelFixed());
                                        startActivity(intent);
                                        finish();
                                    }
                                }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                showGravity("Database error: " + e.getMessage(), Toast.LENGTH_LONG);
                                Log.e("ChannelCreation", "Database error: " + e.getMessage());
                            }
                        });
                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showGravity("Database error: " + e.getMessage(), Toast.LENGTH_LONG);
                        Log.e("ChannelCreation", "Database error: " + e.getMessage());
                    }
                });
            }
        });
    }

    private void showJoinToChannelDialog(final ChannelItem item) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(CreateChannelActivity.this);
        builder.setMessage(R.string.channelExistMessage)
            .setTitle(R.string.channelExistTitle)
            .setNegativeButton(R.string.dialogCancel, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {}
            })
            .setPositiveButton(R.string.dialogSubmit, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                    Intent intent = new Intent(CreateChannelActivity.this, ChatActivity.class);
                    intent.putExtra("chatId", item.getTitle());
                    startActivity(intent);
                    finish();
                }
            });
        AlertDialog dialog = builder.show();
    }

    private void showGravity(String text, int type) {
        Toast toast = Toast.makeText(getApplicationContext(), text, type);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    @Override
    public void onStop() {
        super.onStop();
        ActivitySaver.saveActivity(getSharedPreferences("X", MODE_PRIVATE), CreateChannelActivity.class);
    }

}
