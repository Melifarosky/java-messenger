package com.chapaev.messenger.activities;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.chapaev.messenger.R;
import com.chapaev.messenger.model.User;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


/**
 * Created by Chapaev on 17.03.2017.
 */

public class RegisterActivity extends LoginActivity {
    private static final String TAG = "RegisterForm";

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);

        final Button button1 = (Button) findViewById(R.id.btnBack);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        final Button button2 = (Button) findViewById(R.id.btnRegister);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                final String email = ((EditText) findViewById(R.id.emailText)).getText().toString();
                final String password = ((EditText) findViewById(R.id.passText)).getText().toString();
                final String nickname = ((EditText) findViewById(R.id.nicknameText)).getText().toString();
                if (email.length() == 0 || password.length() == 0 || nickname.length() == 0) return;
                findViewById(R.id.errorMsg).setVisibility(View.GONE);
                findViewById(R.id.registerMsg).setVisibility(View.GONE);
                mAuth.createUserWithEmailAndPassword(email, password)
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (!task.isSuccessful()) {
                                try {
                                    Toast.makeText(RegisterActivity.this, task.getResult().toString(),
                                            Toast.LENGTH_LONG).show();
                                } catch (Exception e) {
                                    Toast.makeText(RegisterActivity.this, e.getLocalizedMessage(),
                                            Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Log.d(TAG, "createUserWithEmail:onComplete:" + task.isSuccessful());
                                FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                DatabaseReference databaseReference = FirebaseDatabase.getInstance().getReference();
                                databaseReference
                                        .child("users")
                                        .child(user.getUid())
                                        .setValue(new User(user, nickname));
                            }

                        }
                    });
            }
        });
    }
}
