package com.chapaev.messenger.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.signature.StringSignature;
import com.chapaev.messenger.R;
import com.chapaev.messenger.handlers.ActivitySaver;
import com.chapaev.messenger.handlers.ImageHandler;
import com.chapaev.messenger.model.User;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.util.Date;

import javax.security.auth.login.LoginException;

import static android.view.KeyEvent.KEYCODE_ENTER;

/**
 * Created by Chapaev on 21.03.2017.
 */

public class ProfileActivity extends AppCompatActivity {
    private static final String TAG = "ProfileActivity";
    private final FirebaseUser firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
    private final DatabaseReference dbRef = FirebaseDatabase.getInstance().getReference();
    private final StorageReference stRef = FirebaseStorage.getInstance().getReference();
    private User user;
    private ImageHandler imageHandler;
    private RequestManager avatarLoader;
    private Object image;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile);

        imageHandler = new ImageHandler();
        //((EditText) findViewById(R.id.profileNameEdit)).clearFocus();
        //((EditText) findViewById(R.id.profileEmailEdit)).clearFocus();


        if (firebaseUser != null) {
            dbRef.child("users").child(firebaseUser.getUid()).addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    user = dataSnapshot.getValue(User.class);
                    if (user != null) {
                        if (user.getEmail() != null)
                            ((TextView) findViewById(R.id.profileEmailEdit)).setText(user.getEmail());
                        if (user.getNickname() != null)
                            ((TextView) findViewById(R.id.profileNameEdit)).setText(user.getNickname());

                        if (user.getAvatarUrl() != null) {
                            avatarLoader = Glide.with(ProfileActivity.this);
                            avatarLoader
                                    .using(new FirebaseImageLoader())
                                    .load(stRef.child("messenger/avatars").child(user.getAvatarUrl()))
                                    .skipMemoryCache(true)
                                    .diskCacheStrategy(DiskCacheStrategy.RESULT)
                                    .signature(new StringSignature(String.valueOf(new Date())))
                                    .crossFade()
                                    .fitCenter()
                                    .error(R.drawable.noimage)
                                    .into((ImageView) findViewById(R.id.profileImage));
                        }
                    }
                    Log.d(TAG, "Successfully loaded user data from database!");
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(TAG, "Database exception = " + databaseError.getMessage());
                    showGravity(databaseError.getMessage(), Toast.LENGTH_LONG);
                }
            });

            ((TextView) findViewById(R.id.profileUid)).setText(firebaseUser.getUid());


            final Button avatarChange = (Button) findViewById(R.id.btnAvatarProfileChange);
            avatarChange.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageHandler.getImage(ProfileActivity.this);
                }
            });

            final EditText nicknameChange = (EditText) findViewById(R.id.profileNameEdit);
            nicknameChange.setOnKeyListener(new View.OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (keyCode == KEYCODE_ENTER) {
                        dbRef.child("users").child(firebaseUser.getUid()).child("nickname").setValue(nicknameChange.getText().toString())
                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                            @Override
                            public void onSuccess(Void aVoid) {
                                Log.d("ProfileActivity", "Successfully changed nickname!");
                                showGravity("Successfully changed nickname!", Toast.LENGTH_SHORT);
                            }
                        }).addOnFailureListener(new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e("ProfileActivity", e.getMessage());
                                showGravity(e.getMessage(), Toast.LENGTH_SHORT);
                            }
                        });
                    }
                    return false;
                }
            });

            final Button btnLogout = (Button) findViewById(R.id.btnLogout);
            btnLogout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    FirebaseAuth.getInstance().signOut();
                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                    intent.addFlags((Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK));
                    startActivity(intent);
                }
            });
        } else {
            try {
                throw new LoginException("[" + TAG + "]: Invalid firebase user data!");
            } catch (LoginException e) {
                e.printStackTrace();
            }
        }

        guiElementListeners();
    }

    private void guiElementListeners() {
        final Button channelButton = (Button) findViewById(R.id.footerChannels);
        channelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ProfileActivity.this, ChannelActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        imageHandler.checkPermissions(requestCode, permissions, grantResults);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        image = imageHandler.setImage((ImageView) findViewById(R.id.profileImage), requestCode, resultCode, data);
        updateAvatar();
    }

    private void updateAvatar() {
        StorageReference imagesRef = stRef.child("messenger/avatars").child(firebaseUser.getUid());
        UploadTask uploadTask;
        if (image instanceof Uri) {
            uploadTask = imagesRef.putFile((Uri) image);
        } else if (image instanceof byte[]) {
            uploadTask = imagesRef.putBytes((byte[]) image);
        } else try {
            throw new Exception("Invalid image instance");
        } catch (Exception e) {
            e.printStackTrace();
            showGravity(e.getMessage(), Toast.LENGTH_LONG);
            return;
        }
        uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                dbRef.child("users").child(firebaseUser.getUid()).child("avatarUrl").setValue(firebaseUser.getUid())
                    .addOnSuccessListener(new OnSuccessListener<Void>() {
                        @Override
                        public void onSuccess(Void aVoid) {
                            showGravity("Successfully changed avatar image!", Toast.LENGTH_LONG);
                            Log.d("Profile", "Successfully changed avatar image!");
                        }
                    }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        showGravity("Database error: " + e.getMessage(), Toast.LENGTH_LONG);
                        Log.e("Profile", "Database error: " + e.getMessage());
                    }
                });
            }
        }).addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception e) {
                showGravity("Storage error: " + e.getMessage(), Toast.LENGTH_LONG);
                Log.e("Profile", "Storage error: " + e.getMessage());
            }
        });
    }

    private void showGravity(String text, int type) {
        Toast toast = Toast.makeText(getApplicationContext(), text, type);
        toast.setGravity(Gravity.BOTTOM|Gravity.CENTER_HORIZONTAL, 0, 0);
        toast.show();
    }

    @Override
    public void onStop() {
        super.onStop();
        if (avatarLoader != null)
            avatarLoader.pauseRequestsRecursive();
        ActivitySaver.saveActivity(getSharedPreferences("X", MODE_PRIVATE), ProfileActivity.class);
    }
}
