package com.chapaev.messenger.model;

import android.support.annotation.Nullable;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Chapaev on 19.03.2017.
 */

@IgnoreExtraProperties
public class Message {
    private String userId;
    private Object timestamp;
    private String message;
    @Exclude
    private String email;
    private String avatarUrl;
    private String imgUrl;

    public Message() {}

    public Message(String userId, String message, @Nullable String avatarUrl, @Nullable String imgUrl) {
        this.userId = userId;
        this.message = message;
        this.avatarUrl = avatarUrl;
        this.imgUrl = imgUrl;
        Map<String, String> timestamp = ServerValue.TIMESTAMP;
        this.timestamp = timestamp;
    }

    @Exclude
    public Long getLongTimestamp() {
        if (timestamp instanceof Long) return (Long) this.timestamp;
        return Long.valueOf(((Map<String, String>)timestamp).get("timestamp"));
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
