package com.chapaev.messenger.model;

import android.support.v7.app.AppCompatActivity;

import com.chapaev.messenger.R;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by Chapaev on 29.03.2017.
 */

public class BadWords{
    private static ArrayList<String> badWords;

    public static void setBadWordsUp(AppCompatActivity activity) throws IOException {
        badWords = new ArrayList<>();
        InputStream inputStream = activity.getResources().openRawResource(R.raw.badwords);

        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader bufferedreader = new BufferedReader(inputreader);
        String line;
            while ((line = bufferedreader.readLine()) != null)
                badWords.add(line);
    }

    public static String getFilteredText(String text) {
        for (String word : badWords) {
            Pattern p = Pattern.compile("\\b" + word + "\\b", Pattern.CASE_INSENSITIVE);
            text = p.matcher(text).replaceAll(new String(new char[word.length()]).replace('\0', '*'));
        }

        return text;
    }
}
