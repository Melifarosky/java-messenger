package com.chapaev.messenger.model;

/**
 * Created by Chapaev on 23.03.2017.
 */

public class Member {
    private boolean isTyping;

    public Member() {}

    public Member(boolean isTyping) {
        this.isTyping = isTyping;
    }

    public boolean isTyping() {
        return isTyping;
    }

    public void setTyping(boolean typing) {
        isTyping = typing;
    }
}
