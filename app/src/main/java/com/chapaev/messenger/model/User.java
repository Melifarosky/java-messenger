package com.chapaev.messenger.model;

import android.support.annotation.Nullable;

import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.IgnoreExtraProperties;

/**
 * Created by Chapaev on 22.03.2017.
 */

@IgnoreExtraProperties
public class User {
    private String nickname;
    private String email;
    private String avatarUrl;

    public User() {}

    public User(FirebaseUser user, @Nullable String nickname) {
        this.nickname = nickname == null ? user.getDisplayName() : nickname;
        email = user.getEmail();
        avatarUrl = "noimage.png";
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String userEmail) {
        email = userEmail;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String aUrl) {
        avatarUrl = aUrl;
    }
}
