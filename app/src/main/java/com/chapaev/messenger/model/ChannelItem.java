package com.chapaev.messenger.model;

import android.text.format.DateFormat;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;
import com.google.firebase.database.ServerValue;

import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by Chapaev on 17.03.2017.
 */

@IgnoreExtraProperties
public class ChannelItem {
    @Exclude
    private String id;
    private long membersCount;
    private String title;
    private String imgUrl;
    private Object timestamp;
    private boolean channelFixed;
    List<Object> members;

    public ChannelItem() {}

    public ChannelItem(String title, String imgUrl) {
        this.title = title;
        this.imgUrl = imgUrl;
        Map<String, String> timestamp = ServerValue.TIMESTAMP;
        this.timestamp = timestamp;
    }

    @Exclude
    public Long getLongTimestamp() {
        if (timestamp == null) return null;
        if (timestamp instanceof Long) return (Long) this.timestamp;
        return Long.valueOf(((Map<String, String>)timestamp).get("timestamp"));
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "#" + title;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public void setImgUrl(String imgUrl) {
        this.imgUrl = imgUrl;
    }

    public Object getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Object timestamp) {
        this.timestamp = timestamp;
    }

    public boolean isChannelFixed() {
        return channelFixed;
    }

    public void setChannelFixed(boolean channelFixed) {
        this.channelFixed = channelFixed;
    }

    public long getMembersCount() {
        return membersCount;
    }

    public void setMembersCount(long membersCount) {
        this.membersCount = membersCount;
    }

    @Exclude
    public String getFormattedTime(){

        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;

        Long tempTimestamp = getLongTimestamp();

        if (tempTimestamp == null) return "";
        long timeDifference = System.currentTimeMillis() - tempTimestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("HH:mm", tempTimestamp).toString()
                : DateFormat.format("dd MMM - HH:mm", tempTimestamp).toString();
    }

    public List<Object> getMembers() {
        return members;
    }

    public void setMembers(List<Object> members) {
        this.members = members;
    }

    public void addMember(Member member) {
        this.members.add(member);
    }
}
