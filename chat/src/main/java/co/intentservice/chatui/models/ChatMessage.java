package co.intentservice.chatui.models;

import android.content.Context;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ProgressBar;

import com.bumptech.glide.DrawableRequestBuilder;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.signature.StringSignature;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.firebase.storage.StorageReference;

import java.util.Date;
import java.util.concurrent.TimeUnit;

import co.intentservice.chatui.R;

public class ChatMessage {
    public enum Type {
        SENT, RECEIVED
    }

    private Context context;
    private String message;
    private long timestamp;
    private StorageReference avatarRef;
    private StorageReference imgRef;
    private Type type;

    public ChatMessage(Context context, String message, StorageReference avatarRef, StorageReference imgRef, long timestamp, Type type){
        this.context = context;
        this.message = message;
        this.timestamp = timestamp;
        this.type = type;
        this.avatarRef = avatarRef;
        this.imgRef = imgRef;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }


    public String getFormattedTime(){
        long oneDayInMillis = TimeUnit.DAYS.toMillis(1); // 24 * 60 * 60 * 1000;
        long timeDifference = System.currentTimeMillis() - timestamp;

        return timeDifference < oneDayInMillis
                ? DateFormat.format("HH:mm", timestamp).toString()
                : DateFormat.format("dd MMM - HH:mm", timestamp).toString();
    }

    public Context getContext() {
        return context;
    }

    @Nullable
    private DrawableRequestBuilder<StorageReference> getLoadedResource(final StorageReference res,
                                                                       final ProgressBar progressBar) {
        if (context == null || res == null) return null;
        return Glide.with(context)
                .using(new FirebaseImageLoader())
                .load(res)
                .skipMemoryCache(true)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .signature(new StringSignature(String.valueOf(new Date())))
                .crossFade()
                .fitCenter()
                .listener(new RequestListener<StorageReference, GlideDrawable>() {
                    @Override
                    public boolean onException(Exception e, StorageReference model, Target<GlideDrawable> target, boolean isFirstResource) {
                        return false;
                    }

                    @Override
                    public boolean onResourceReady(GlideDrawable resource, StorageReference model, Target<GlideDrawable> target, boolean isFromMemoryCache, boolean isFirstResource) {
                        if (progressBar != null)
                            progressBar.setVisibility(View.GONE);
                        return false;
                    }
                })
                .error(R.drawable.noimage);
    }

    public DrawableRequestBuilder<StorageReference> getLoadedAvatar(ProgressBar progressBar) {
        return getLoadedResource((StorageReference) avatarRef, progressBar);
    }

    public DrawableRequestBuilder<StorageReference> getLoadedImage(ProgressBar progressBar) {
        return getLoadedResource((StorageReference) imgRef, progressBar);
    }

    public boolean hasMessageImage() {
        return imgRef != null;
    }
}
